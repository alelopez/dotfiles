"=============
"=== VIMRC ===
"=============

source ~/.vim/basic.vim
source ~/.vim/plugins.vim
source ~/.vim/functions.vim
source ~/.vim/commands.vim

source ~/.vim/general.vim
source ~/.vim/remaps.vim
source ~/.vim/highlighting.vim

" Backup, history, swap, and undo options.
set backup " Enable backups
set backupdir=~/tmp/vim/backup// " Backup file directory
set directory=~/tmp/vim/swap// " Swap file directory
set undodir=~/tmp/vim/undo// " Undo file directory
set undofile " Enable persistent undo
set viminfo+=n~/tmp/vim/viminfo
