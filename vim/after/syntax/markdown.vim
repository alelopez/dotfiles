syntax case match

"syn match mkdTodo /TODO:\?/ containedin=htmlH[1-6],mkdNonListItemBlock
syn match mkdTodo /- TODO:\?/
hi def link mkdTodo  Todo

syntax case ignore
