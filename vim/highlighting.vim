"====================
"=== HIGHLIGHTING ===
"====================

"===============
"=== General ===
"===============
"hi Folded term=bold cterm=NONE ctermfg=White
hi Folded cterm=NONE


"==================
"=== Javascript ===
"==================
" Strings:
hi jsString ctermfg=222
hi jsTemplateString ctermfg=222
hi jsTemplateExpression ctermfg=222

hi jsNumber ctermfg=141
hi jsFloat ctermfg=141
hi jsThis ctermfg=141
hi jsStorageClass ctermfg=117

hi jsBooleanTrue ctermfg=141
hi jsBooleanFalse ctermfg=141
hi jsUndefined ctermfg=141
hi jsNull ctermfg=141
hi jsNan ctermfg=141

" Functions:
hi jsFunction ctermfg=117
hi jsFuncName ctermfg=208
hi jsFuncArgs ctermfg=214
hi jsFuncArgCommas ctermfg=117
hi jsFuncCall ctermfg=208

hi jsBrackets ctermfg=33
hi jsBracket ctermfg=7
hi jsOperator ctermfg=106

hi jsReturn ctermfg=106

" Globales:
hi jsCommentTodo ctermfg=198
hi jsGlobalObjects ctermfg=117
hi jsNoise ctermfg=117
hi jsParensError ctermfg=231 ctermbg=196
hi jsBuiltins ctermfg=117

" Classes:
hi jsClassKeywords ctermfg=117
hi jsClassNoise ctermfg=117
hi jsClassDefinition ctermfg=178

" Objects:
hi jsPrototype ctermfg=106
hi jsObjectStringKey ctermfg=214
hi jsObjectSeparator ctermfg=117


" Modules:
hi jsModuleKeywords ctermfg=106
hi jsModuleOperators ctermfg=106
hi jsModuleExportContainer ctermfg=106

" Statemets:
hi jsStatement ctermfg=106
hi jsConditional ctermfg=106
hi jsRepeat ctermfg=106
hi jsLabel ctermfg=106
hi jsTry ctermfg=106
hi jsFinally ctermfg=106
hi jsCatch ctermfg=106
hi jsException ctermfg=106
hi jsAsyncKeyword ctermfg=106
hi jsSwitchColon ctermfg=106

" HTML:
hi jsHtmlEvent ctermfg=106


"================
"=== Markdown ===
"================
" Enlaces:
hi markdownLinkText ctermfg=106
hi markdownUrl ctermfg=106

" Codigo:
"hi markdownCodeDelimiter ctermfg=106


"===============
"=== Vimwiki ===
"===============
hi VimwikiLink ctermfg=222 cterm=NONE
hi VimwikiWeblink1 ctermfg=222 cterm=NONE
