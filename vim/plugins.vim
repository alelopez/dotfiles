"===============
"=== PLUGINS ===
"===============
" https://github.com/junegunn/vim-plug

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')
" Vim interfaz:
    Plug 'itchyny/lightline.vim'
    Plug 'taohexxx/lightline-buffer'
    Plug 'mhinz/vim-startify'

" Plugins gestión:
    Plug 'tpope/vim-vinegar'
    Plug 'mbbill/undotree'
    "Plug 'tpope/vim-surround'
    Plug 'junegunn/goyo.vim'
    Plug 'junegunn/limelight.vim'
    Plug 'junegunn/fzf.vim'
    "Plug 'tpope/vim-fugitive'
    "Plug 'jremmen/vim-ripgrep'

" Plugins sintáxis:
    Plug 'pangloss/vim-javascript'
    Plug 'mxw/vim-jsx'
    Plug 'ap/vim-css-color'
    Plug 'elzr/vim-json'
    Plug 'chr4/nginx.vim'
    Plug 'groenewege/vim-less'
    Plug 'preservim/vim-markdown'
    "Plug 'hdima/python-syntax'
    Plug 'Glench/Vim-Jinja2-Syntax'
    Plug 'cespare/vim-toml'

" Plugins correctores
    Plug 'vim-syntastic/syntastic'
    "Plug 'w0rp/ale'
    Plug 'mtscout6/syntastic-local-eslint.vim'
call plug#end()


"=== Netrw ===
" Configuración de netrw:
"let g:netrw_home = $HOME."/.cache"
let g:netrw_altv = 1
let g:netrw_banner = 0
let g:netrw_browse_split = 4
let g:netrw_liststyle = 3
let g:netrw_winsize = -30
"let g:netrw_bufsettings = 'noma nomod nu nobl nowrap ro rnu'  " --> I want line numbers on the netrw buffer

"=== LightLine ===
let g:lightline = {
    \ 'tabline': {
        \ 'left': [
            \ [ 'bufferinfo' ],
            \ [ 'bufferbefore', 'buffercurrent', 'bufferafter' ],
        \ ],
        \ 'right': [
            \ [ 'buffers' ],
        \ ],
    \ },
    \ 'colorscheme': 'pastelblue',
    \ 'active': {
        \ 'left': [
            \ [ 'mode', 'paste', 'spell' ],
            \ [ 'gitbranch', 'readonly', 'filename', 'modified' ]
        \ ],
        \ 'right': [
            \ [ 'lineinfo' ],
            \ [ 'percent', 'wordcount' ],
            \ [ 'fileformat', 'fileencoding', 'filetype' ]
        \ ],
    \ },
    \ 'component': {
        \ 'buffers': 'buffers',
        \ 'readonly': '%{&filetype=="help"?"":&readonly?"⭤":""}',
        \ 'modified': '%{&filetype=="help"?"":&modified?"+":&modifiable?"":"-"}',
    \ },
    \ 'component_expand': {
        \ 'buffercurrent': 'lightline#buffer#buffercurrent2',
    \ },
    \ 'component_type': {
        \ 'buffercurrent': 'tabsel',
    \ },
    \ 'component_function': {
        \ 'bufferbefore': 'lightline#buffer#bufferbefore',
        \ 'bufferafter': 'lightline#buffer#bufferafter',
        \ 'bufferinfo': 'lightline#buffer#bufferinfo',
        \ 'wordcount': 'WordCount',
    \ }
\ }


"=== Undotree ===
let g:undotree_CustomUndotreeCmd = 'vertical 32 new'
let g:undotree_CustomDiffpanelCmd= 'belowright 12 new'


"=== Syntastic ===
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_mode_map = {
    \'mode': 'active',
    \'active_filetypes': [],
    \'passive_filetypes': ['html', 'scss']}
let g:syntastic_loc_list_height = 3 " Make window small
let g:syntastic_javascript_checkers = ['eslint']
let g:syntastic_aggregate_errors = 1
let g:syntastic_error_symbol = ">>"
let syntastic_style_error_symbol = ">>"
let g:syntastic_warning_symbol = "!"
let syntastic_style_warning_symbol = "!"
"let g:syntastic_python_checkers = ['flake8']
"let g:syntastic_python_flake8_args = '--ignore="E501,E302,E261,E701,E241,E126,E127,E128,W801"'
"let g:syntastic_javascript_checkers = ['jshint']
"let g:syntastic_html_tidy_ignore_errors=['<link>', '<script>']
"let g:syntastic_python_checkers = ['pyflakes']
"let g:syntastic_python_pylint_post_args='--disable=C0103,C0111' " Tame pylint


"=== LimeLight ===
let g:limelight_conceal_ctermfg = 'gray'
let g:limelight_conceal_ctermfg = 240
let g:limelight_default_coefficient = 0.7


"=== Goyo ===
function! s:goyo_enter()
    "silent !tmux set status off
    "silent !tmux list-panes -F '\#F' | grep -q Z || tmux resize-pane -Z
    let g:goyo_width=104
    set noshowmode
    set noshowcmd
    set scrolloff=999
    Limelight
    let b:quitting = 0
    let b:quitting_bang = 0
    autocmd QuitPre <buffer> let b:quitting = 1
    cabbrev <buffer> q! let b:quitting_bang = 1 <bar> q!
endfunction
function! s:goyo_leave()
    "silent !tmux set status on
    "silent !tmux list-panes -F '\#F' | grep -q Z && tmux resize-pane -Z
    " Quit Vim if this is the only remaining buffer
    if b:quitting && len(filter(range(1, bufnr('$')), 'buflisted(v:val)')) == 1
        if b:quitting_bang
            qa!
        else
            qa
        endif
    endif
    set showmode
    set showcmd
    set scrolloff=5
    Limelight!
    set background=dark
    au colorscheme * hi clear SpellBad SpellCap
        \| hi SpellBad ctermfg=red
        \| hi SpellCap cterm=none ctermfg=yellow
        "\| hi ExtraWhitespace ctermbg=red
        "\| match ExtraWhitespace /\s\+$/
    colorscheme solarized
endfunction
autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()


"=== fzf ===
set rtp+=~/.fzf
"nnoremap <silent><leader>b :Buffers<CR>
nnoremap <silent><leader>h :History<CR>
nnoremap <silent><leader>o :Files<CR>
nnoremap <silent><leader>O :GFiles<CR>


"=== preservim/vim-markdown ===
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_no_extensions_in_markdown = 1
let g:vim_markdown_conceal_code_blocks = 0
let g:vim_markdown_frontmatter = 1
let g:vim_markdown_math = 1
let g:vim_markdown_auto_insert_bullets = 0
let g:vim_markdown_new_list_item_indent = 0
let g:markdown_fenced_languages = ['html', 'js=javascript', 'css', 'less', 'sass', 'scss', 'python', 'bash=sh', 'sh']
au FileType markdown
            \ hi htmlLink ctermfg=7
            \| hi htmlString ctermfg=10
            \| hi Delimiter ctermfg=7


"=== JSX ===
let g:jsx_ext_required = 1
