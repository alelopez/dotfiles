"==============
"=== REMAPS ===
"==============
let mapleader=","

imap <F1> <Nop>
cmap <F1> <Nop>
nmap <F1> <Nop>
nmap K <Nop>
nmap q <Nop>
nmap Q <Nop>
nmap ,q <Nop>
imap <MiddleMouse> <Nop>
cmap <MiddleMouse> <Nop>
nmap <MiddleMouse> <Nop>
cmap ZZ <Nop>
nmap ZZ <Nop>
cmap Z <silent> Z <Nop>
nmap Z <silent> Z <Nop>
nmap - <silent> <Nop>
vmap - <Nop>
noremap <C-o> <Nop>
imap <C-h> <Nop>
cmap <C-h> <Nop>
nmap <C-h> <Nop>
imap <C-l> <Nop>
cmap <C-l> <Nop>
nmap <C-l> <Nop>
nmap s <Nop>
:command! WQ wq
:command! Wq wq
:command! W w
:command! Q q
:command! -bar -bang Q quit<bang>
:command! -nargs=1 -complete=help H :enew | :setl buftype=help | :setl nobuflisted | :keepalt h <args>
"imap ZZ <Nop>
":command -bang Q quit<bang>
":command! X x


"=== Desplazamiento ===
inoremap <C-k> <Up>
inoremap <C-j> <Down>
inoremap <C-h> <Left>
inoremap <C-l> <Right>
inoremap <Up> <Nop>
inoremap <Down> <Nop>
inoremap <Left> <Nop>
inoremap <Right> <Nop>
noremap <Up> <Nop>
noremap <Down> <Nop>
noremap <Left> <Nop>
noremap <Right> <Nop>
noremap <C-k> <Up>
noremap <C-j> <Down>
noremap <C-h> <Left>
noremap <C-l> <Right>
noremap <S-k> <Up>
noremap <S-j> <Down>
noremap <S-h> <Left>
noremap <S-l> <Right>
noremap <C-m> <Nop>
noremap <C-n> <Nop>

"noremap <S-Tab> <Tab>
inoremap <S-Tab> <C-V><Tab>
" Desplazamiento entre ventanas:
nnoremap <C-w>k <C-W><C-K>
nnoremap <C-w>j <C-W><C-J>
nnoremap <C-w>h <C-W><C-H>
nnoremap <C-w>l <C-W><C-L>
nnoremap <Tab> <C-W><C-W>
nnoremap <C-w>r <C-W>R          " Cambia la posición de las ventanas


"=== Buffers ===
nnoremap <S-Tab> :buffers<CR>:buffer<Space>
" Cambia al buffer seleccionado:
nnoremap <silent><leader>1 :1b<CR>
nnoremap <silent><leader>2 :2b<CR>
nnoremap <silent><leader>3 :3b<CR>
nnoremap <silent><leader>4 :4b<CR>
nnoremap <silent><leader>5 :5b<CR>
nnoremap <silent><leader>6 :6b<CR>
nnoremap <silent><leader>7 :7b<CR>
nnoremap <silent><leader>8 :8b<CR>
nnoremap <silent><leader>9 :9b<CR>
nnoremap <silent><leader>0 :10b<CR>
nnoremap <silent><leader>x :Bclose<CR>  " Cierra el buffer activo sin cerrar la ventana
nnoremap <silent><leader>q :close<CR>   " Cierra la ventana activa conservando los buffers
nnoremap <silent><leader>Q <C-w>o       " Cierra todas las menos la activa conservando los buffers
"nnoremap <silent><leader>x :Bclose<cr>:tabclose<cr>gT

nnoremap <silent><leader><CR> :Startify<CR>
nnoremap <silent><leader>w :setl wrap!<CR>
nnoremap <silent><leader>e :Lexplore<CR>
nnoremap <silent><leader>E :Explore<CR>
nnoremap <silent><leader>R :retab<CR>
nnoremap <silent> "" :registers "0123456789abcdefghijklmnopqrstuvwxyz*+.<CR>
nnoremap <silent><leader>a :nohlsearch<CR>
nnoremap <silent><leader>z :UndotreeToggle<CR>
nnoremap <silent><leader>u :UndotreeToggle<CR>
nnoremap <silent><leader>p :setl paste!<CR>
nnoremap <silent><leader>n :setl number! list!<CR>
nnoremap <silent><leader>N :vertical new<CR>
nnoremap <silent><leader>m :setl rnu!<CR>
nnoremap <silent><leader>g :Goyo<CR>
nnoremap <silent><leader>bg :call ToggleBackground()<CR>
"nnoremap <silent><leader>Y :let &background = (&background == 'dark' ? 'light' : 'dark')<CR>
nnoremap [ #
nnoremap ] *
"nnoremap <silent><leader>k :setl list!<CR>
"nnoremap <silent> <leader>e :VimFilerExplorer -winwidth=25 -split -simple -toggle -no-quit<cr>
"nnoremap <silent><leader>c :copen<CR>
"nnoremap <silent><leader>x :cclose<CR>
"nnoremap <silent><leader>P "*P

"=== Edición ===
noremap! <C-BS> <C-w>
imap <C-d> <C-[>diwi        " Borra la última palabra
inoremap <C-o> <Esc>o
nnoremap << >>
xnoremap << >>
nnoremap >> <<
xnoremap >> <<
"inoremap <C-i> <Nop>       " <C-i> es como <Tab>
"inoremap <C-i> <Esc>O
nnoremap u <Nop>
nnoremap U <Nop>
nnoremap <C-u> :u<CR>
nnoremap <leader>r :%s/\<<c-r><c-w>\>//gc<left><left><left>
"inoremap ( ()<left>
"inoremap { {}<left>
"inoremap [ []<left>
"inoremap " ""<left>
"inoremap ' ''<left>
"inoremap (; (<CR>);<C-c>O
"inoremap (, (<CR>),<C-c>O
"inoremap {; {<CR>};<C-c>O
"inoremap {, {<CR>},<C-c>O
"inoremap [; [<CR>];<C-c>O
"inoremap [, [<CR>],<C-c>O

"=== Completado ===
inoremap <C-b> <C-x><C-k>
"inoremap <C-f> <C-x><C-f>
inoremap <NUL> <C-x><C-n>
inoremap <expr> <c-x><c-f> fzf#vim#complete#path('rg --files')
inoremap <expr> <C-f> fzf#vim#complete#path('rg --files')

"=== Búsqueda ===
"set grepprg=rg\ --vimgrep\ --no-heading\ --smart-case
"nnoremap <leader>F :Rg! <C-R><C-W><CR>
"command! -bang -nargs=* Rg call fzf#vim#grep("rg --column --line-number --no-heading --color=always --smart-case ".shellescape(<q-args>), 1, {'options': '--delimiter : --nth 4..'}, <bang>0)
command! -bang -nargs=* Rg
  \ call fzf#vim#grep(
  \   'rg --column --line-number --no-heading --color=always --smart-case '
  \  . (len(<q-args>) > 0 ? <q-args> : '""'), 1,
  \   <bang>0 ? fzf#vim#with_preview('up:60%')
  \           : fzf#vim#with_preview('right:50%:hidden', '?'),
  \   <bang>0)
nnoremap <silent><leader>b :Rg <C-R><C-W><CR>
