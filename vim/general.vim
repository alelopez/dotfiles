"===============
"=== GENERAL ===
"===============

"=== Configuración personal:
"filetype plugin indent on
set nocompatible
"filetype off

" Definimos la codificación:
set encoding=utf-8 fileencoding=utf-8
"syntax off
syntax enable
set modeline
set modelines=5

" Definimos el algoritmo de encriptación:
set cm=blowfish2

" Configuración de idiomas:
set spelllang=es
"set spell

set completeopt=longest,menuone

set noshowmode
set laststatus=2
set ttimeoutlen=50
"set hidden
set tabstop=4 softtabstop=4 shiftwidth=4 expandtab smarttab list cursorline
"set ignorecase
"set backspace=indent,eol,start " Make Backspace key work correctly
"set listchars=eol:$,tab:>-,trail:·,extends:»,precedes:«
set listchars=eol:¬,tab:>-,trail:·,extends:»,precedes:«
set nowrap
let g:solarized_termcolors=256
set background=dark
au colorscheme * hi clear SpellBad SpellCap
            \| hi SpellBad ctermfg=red
            \| hi SpellCap cterm=none ctermfg=yellow
            \| hi MatchParen cterm=none ctermbg=162 ctermfg=white
            "\| hi SpellBad ctermfg=white ctermbg=162
            "\| hi ExtraWhitespace ctermbg=red
            "\| match ExtraWhitespace /\s\+$/
colorscheme solarized
hi CursorLineNr cterm=none

" Numeración de líneas:
set number
set numberwidth=5
set nojoinspaces " Two spaces between sentences == bad
set scrolloff=3 " Keep cursor on screen while scrolling vertically
set clipboard=unnamed
"set statusline=%<%F%h%m%r%h%w%y\ [L:\ %l\/%L]\ [Co:\ %c%V]\ [Ch:\ %o]\ [%%:\ %P]

" Change directory to the current buffer when opening files:
set autochdir

" Cambio de orden al abrir ventanas:
set splitbelow
set splitright

" Command-line completion options.
"set wildmenu " Show possible matches for command-line completion

" Command-line completion ignore options.
set wildignore+=*.dll,*.exe,*.manifest,*.o,*.obj " Compiled objects
set wildignore+=*.spl " Compiled spelling lists
set wildignore+=*.bmp,*.gif,*.jpeg,*.jpg,*.png " Image files
set wildignore+=*.aux,*.out,*.toc " LaTeX intermediate files
set wildignore+=*.DS_Store " OS X files
set wildignore+=*.pyc " Python bytecode
set wildignore+=.git,.hg,.svn " Version control files
set wildignore+=*.sw? " Vim swap files

set conceallevel=2

" Configuraciones por extensión:
au BufRead,BufNewFile *.py setl filetype=python
au BufRead,BufNewFile *.md setl filetype=markdown
au BufRead,BufNewFile *.json setl filetype=json
"au BufRead,BufNewFile *.js setl filetype=javascript
au BufRead,BufNewFile *.less setl filetype=less
au BufRead,BufNewFile *.handlebars setl filetype=html
au BufRead,BufNewFile *.hbt setl filetype=html

au BufRead,BufNewFile *.{yaml,yml} set filetype=yaml
au FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

"au FileType python,sh setl fo=croq ts=4 sw=4
au FileType python setl textwidth=79
    \| setl colorcolumn=+1
    \| setl expandtab
    \| setl autoindent
    \| setl fileformat=unix
au FileType javascript setl ts=4 sts=4 sw=4

au BufRead,BufNewFile *.epub call zip#Browse(expand("<amatch>"))
au BufRead,BufNewFile TODO,*.TODO,*.todo,tareas,TAREAS,tareas.txt setl filetype=todo
