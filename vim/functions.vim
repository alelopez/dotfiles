"=================
"=== FUNCIONES ===
"=================

" TODO Crear directorio para separar las funciones: funciones/{1.vim,2.vim,...}

function! CambioIndentacion(anterior, nueva)
    let &l:ts = a:anterior
    let &l:sts = a:anterior
    setl noet
    retab!
    let &l:ts = a:nueva
    let &l:sts = a:nueva
    setl et
    retab
endfunction

for d in glob('~/.vim/spell/*.add', 1, 1)
    if filereadable(d) && (!filereadable(d . '.spl') || getftime(d) > getftime(d . '.spl'))
        "silent exec 'mkspell! ' . fnameescape(d)
        exec 'mkspell! ' . fnameescape(d)
    endif
endfor

function! ToggleBackground()
  if &background == "dark"
    syntax on
    set background=light
    hi CursorLineNr cterm=none
  else
    syntax on
    set background=dark
    hi CursorLineNr cterm=none
  endif
endfunction

"function! VentanaParalela(numero)
"    let numerobuffer = a:numero
"    if bufexists(numerobuffer)
"        :vert sb numerobuffer
"    endif
"endfunction

" Toggle Vexplore with Ctrl-E
"function! ToggleVExplorer()
"  if exists("t:expl_buf_num")
"      let expl_win_num = bufwinnr(t:expl_buf_num)
"      if expl_win_num != -1
"          let cur_win_nr = winnr()
"          exec expl_win_num . 'wincmd w'
"          close
"          exec cur_win_nr . 'wincmd w'
"          unlet t:expl_buf_num
"      else
"          unlet t:expl_buf_num
"      endif
"  else
"      exec '1wincmd w'
"      Vexplore
"      let t:expl_buf_num = bufnr("%")
"  endif
"endfunction


function! WordCount()
    let currentmode = mode()
    if !exists("g:lastmode_wc")
        let g:lastmode_wc = currentmode
    endif
    " if we modify file, open a new buffer, be in visual ever, or switch modes
    " since last run, we recompute.
    if &modified || !exists("b:wordcount") || currentmode =~? '\c.*v' || currentmode != g:lastmode_wc
        let g:lastmode_wc = currentmode
        let l:old_position = getpos('.')
        let l:old_status = v:statusmsg
        execute "silent normal g\<c-g>"
        if v:statusmsg == "--No lines in buffer--" || v:statusmsg == "--No hay líneas en el búfer--"
            let b:wordcount = 0
        else
            let s:split_wc = split(v:statusmsg)
            if index(s:split_wc, "Selected") < 0
                let b:wordcount = str2nr(s:split_wc[11])
            else
                let b:wordcount = str2nr(s:split_wc[5])
            endif
            let v:statusmsg = l:old_status
        endif
        call setpos('.', l:old_position)
        return b:wordcount
    else
        return b:wordcount
    endif
endfunction
