" Vim syntax file
" Language: TODO

if exists("b:current_syntax")
    finish
endif

syn match todoTopLevel /^[^x\- ].*$/
syn match todoHeaderDelim /^[\-\=]*$/
syn match todoLineItem /^\s*[\-\*] .*$/
syn match todoImportantItem /^\s*+ .*$/
syn match todoInProgressItem /^\s*> .*$/
syn match todoUrgentItem /\(^\s*\)\@<=! .*$/
syn match todoNoteItem /^\s*\. .*$/
syn match todoQuestionItem /^\s*? .*$/
syn match todoDoneItem /^\s*x .*$/
"syn match todoUrgentItem /^\s*[\-\*] .*!.*$/
"syn match todoQuestionItem /^\s*[\-\*] .*?.*$/

hi link todoTopLevel Function
hi link todoHeaderDelim Comment
hi link todoLineItem Normal
hi link todoDoneItem Comment
hi link todoImportantItem Title
"hi link todoQuestionItem Special
"hi link todoNoteItem SpecialComment
"hi link todoInProgressItem String

hi todoInProgressItem ctermfg=10
hi todoUrgentItem ctermfg=255 ctermbg=203
hi todoImportantItem ctermfg=202
hi todoQuestionItem ctermfg=178
hi todoNoteItem ctermfg=162

let b:current_syntax = "todo"
