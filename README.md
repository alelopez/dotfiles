Dotfiles

## Instalación desde cero:
- Clonar el repositorio en la raiz del usuario.
- Ejecutar los siguientes scripts:
    - `./utilidades/welcome-scripts/welcome--basic`
    - `./utilidades/welcome-scripts/welcome--i3`
    - `./utilidades/welcome-scripts/welcome--vim_install`
    - `./utilidades/welcome-scripts/welcome--fonts`
    - `./utilidades/welcome-scripts/welcome--ranger`


## Utilidades
Para cambiar el icono de Dropbox:
`./utilidades/welcome-scripts/welcome--dropbox_icons grey`
