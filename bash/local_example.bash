#!/usr/bin/env bash

# variables
note_dir="${HOME}/Dropbox/personal--notas"
dot_dir="${HOME}/dotfiles/sh"
basura="${HOME}/tmp/basura"
basura_local="${HOME}/tmp/basura_local"
dotfiles="${HOME}/dotfiles"
dropbox="${HOME}/Dropbox"
kapture="${dropbox}/kapture"
utilidades="${kapture}/utilidades"
documentacion="${kapture}/documentacion"

# alias
