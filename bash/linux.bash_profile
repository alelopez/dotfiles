# vim: filetype=sh

if [ -n "$BASH_VERSION" ]; then
    if [ -f "$HOME/.bashrc" ]; then
        . "$HOME/.bashrc"
    fi
fi

if [[ -x /usr/local/bin/firefox-dev ]]; then
    export BROWSER=firefox-dev
elif [[ -x /usr/local/bin/vimb ]]; then
    export BROWSER=vimb
fi

if [[ -d "${HOME}/.nvm" ]]; then
    export NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
fi
