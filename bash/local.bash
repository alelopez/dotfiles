#!/usr/bin/env bash

# variables
basura="${HOME}/tmp/basura"
basura_local="${HOME}/tmp/basura_local"
dotfiles="${HOME}/dotfiles"
documentos="${HOME}/Documents"
compartido="${documentos}/compartido"
note_dir="${compartido}/personal--notas"
temporal="${HOME}/tmp"
